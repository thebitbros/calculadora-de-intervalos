# Calculadora de intervalos

## Sinopsis

Esta calculadora es parte de un módulo que fue construido para calcular un intevalo de horas dentro de un rango de días (e.g intervalos de 3 horas en un rango de 4 días, que es igual a 12 horas). Al ser parte de un módulo, con la ayuda de otras funciones, esta calculadora es capaz de ignorar ciertos intervalos (previamente declarados), días (como el domingo, por ejemplo), entre otras cosas. El código viene comentado para más información.

## Licencia

GPL v3.0