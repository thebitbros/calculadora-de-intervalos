<?php

function calculate_Range ($room, $days, $dayname, $init_hr, $end_hr) {
  
		/*
			 DESCRIPCIÓN:
			 La función calculate_Range() recibe un tipo de área a rentar, los días a rentar, el nombre del día de
			 comienzo, una hora de inicio y una hora de término. Calcula por medio de una matriz una suma de 
			 precios que se calcula contando cada día subsecuente del día indicado (Si la función recibe un 1 en
			 el campo de $days, sólo calculará valores en el intervalo de ese día).
			 FUNCIONES INCLUÍDAS:
			 Esta función hace uso de la función getPrice().
			 RETORNO:
			 Regresará null si se ingresa un 0 en el número de días o sí es un sólo día y la hora de comienzo es
			 mayor a la de término.
			 Regresará un valor numérico entero con el valor representativo de los horarios calculados para los
			 intervalos seleccionados de la forma:
			 (Día de inicio, Hora de inicio) -[intervalo]-> (Día final, Hora de término)
		*/
	
//Validar la información antes de comenzar: Si el valor de la variable $days es igual a 1 y el valor de la variable $init_hr es mayor que el de la variable $end_hr, o el valor de la variable $days es igual a 0 por alguna razón, se llama a la función validateData() con el parámetro "hour".
  if ($days == 1 && $init_hr > $end_hr || $days == 0) {  validateData('hour');  }
  
/*

Arreglos de tipos de hora según el día. Cada arreglo debe contener en número de índices el rango total 
posible de horas de renta (que es 14, porque se puede rentar desde las 7:00 hasta las 21:00). El primer 
índice representa la primera hora en donde es posible rentar (7:00), y el valor de dicho índice es el precio 
individual de esa hora en particular, y así sucesivamente con los demás índices. El precio asociado a cada 
letra se puede encontrar en la función getPrice($room,$type).

*/
//Lunes a viernes.
  $LV = Array("A","B","B","B","B","B","B","B","B","B","B","A","A","A");
//Sábado.
  $S = Array("X","X","A","A","A","A","A","A","C","C","C","C","C","X");
//Domingo.
  $D = Array("X","X","X","X","X","X","X","X","X","X","X","X","X","X");
//Horas individuales de días laborales (L - V).
  $HR = Array("A","A","A","A","A","A","A","A","A","A","A","A","A","A");
	
//En esta matriz compuesta se relaciona cada día con su arreglo de tipos de hora correspondiente.
  $DAY_HOUR = Array("Lun"=>$LV,"Mar"=>$LV,"Mie"=>$LV,"Jue"=>$LV,"Vie"=>$LV,"Sab"=>$S,'Dom'=>$D,"HR"=>$HR);
	
//Bloque para comenzar a iterar el arreglo en el día correcto.
//EL arreglo $DAYS contiene todos los días de la semana.
  $DAYS = Array("Lun","Mar","Mie","Jue","Vie","Sab",'Dom');
//El arreglo $WORKDAYS contiene todos los días laborales.
  $WORKDAYS = Array("Lun","Mar","Mie","Jue","Vie");
//La variable $start_iter contiene como valor a la función de arrays integrada array_search(), teniendo esta como parámetros a la variable $dayname (que a su vez es un parámetro de la función actual) y al arreglo $DAYS.
  $start_iter = array_search($dayname,$DAYS);
	
//Comienza bloque de cálculo:
//La variable $price siempre tiene que poseer un valor de 0 por defecto.
  $price = 0;
  
//Si el valor contenido en la variable $dayname se encuentra dentro del arreglo $WORKDAYS y el valor de la variable $init_hr es igual al valor de la variable $end_hr menos 1, se está hablando entonces de una sola hora de renta, y se le asigna el valor "HR" a la variable $dayname.
  if(in_array($dayname,$WORKDAYS) && $init_hr == $end_hr - 1) { $dayname = "HR"; }

/*

Cálculo de día y hora sencillo (L-V | 8-18): Si el valor de la variable $init_hr es igual al de la 
variable $end_hr menos 1 y el valor de la variable $dayname es igual a "HR", se invoca a la función 
getPrice($room,$type) con los parámetros necesarios. Básicamente, se pasan como parámetro a dicha 
función el valor de la variable $room (pues el precio de renta de ambas salas es diferente por 
completo), el valor de índice "HR" en $DAY_HOUR[] (se necesita relacionar el nombre del índice con
el arreglo que le corresponde), pero, hay un parámetro de sobra aparentemente: [$init_hr - 7]. Lo que
se está haciendo en este caso es que al índice que se está buscando en el arreglo $DAY_HOUR[] se le asigna
su propio número de índice; por ende, en este caso el resultado de $DAY_HOUR["HR"][$init_hr - 7] sería
$HR[$init_hr - 7], que, si por ejemplo, el valor de $init_hr es igual a 2, quedaría $HR[-5].

*/
	if($init_hr == $end_hr-1 && $dayname == "HR") { $price = getPrice($room,$DAY_HOUR["HR"][$init_hr - 7]); }
	
//Cálculo del intervalo.
  else {
		
//Se decrementa la hora final porque se calcula por intervalos de 1 hora. Ejemplo: Se calcula de 10 a 11, utilizando sólamente el 10, que cuenta como 1 hora.
    $end_hr--;
		
/*

Básicamente, el cálculo se lleva a cabo de la siguiente manera:

En un loop FOR, el valor de la variable $i es igual a 0, y se iterará mientras esta sea menor al valor de
$days; por cada iteración, se aumentará en 1 el valor de las variables $i y $start_iter. Dentro de este 
loop FOR hay una condición IF y un ELSE; la condición IF se llena únicamente si el valor de la variable $i 
es igual a 0 (eso sucede si se empezó a iterar o se reestableció el valor de la variable $start_iter a -1, 
casi terminando el estatuto ELSE). En este estatuto, a la variable $j (que se creará únicamente para 
satisfacer las necesidades de este loop FOR) se le asigna el valor de la variable $init_hr menos 7 (se le 
restan 7 para que tanto el valor de las variables $init_hr y $end_hr puedan llegar a coincidir y así saber 
que no restan horas por iterar), y se iterará mientras el valor de la variable $j sea menor a 14 (el número 
total de horas dentro de cualquiera de los arreglos de tipos de hora), y por cada iteración se incrementará 
en 1 el valor de la variable $j. En cada iteración, se llama a la función getPrice($room,$type) con los 
parámetros correspondientes a la situación (abajo se explicará en cada caso el por qué de esos valores). 
Finalmente, si el valor de la variable $j es igual al valor de la variable $end_hr menos 7 y el valor de 
la variable $i es igual al valor de la variable $days menos uno, el loop se rompe, quedando el valor de 
la variable $price como el resultado de la operación.

*/
    for($i = 0; $i < $days; $i++, $start_iter++) {
      if($i == 0) {
        for($j = $init_hr - 7; $j < 14; $j++) {
					
/*

Aquí, en este caso, Se pasan como parámetros el valor de la variable $room (para que se pueda establecer 
de qué sala se trata el asunto) y un valor de índice para el arreglo $DAY_HOUR[], que será el valor de 
la variable $dayname (que es un parámetro de esta función tal cual) y a ese índice mismo se le otorgará 
un número de índice propio para su uso, que en este caso será el valor de la variable $j. De este modo, 
si por ejemplo, el valor de la variable $dayname es "Mar" y el de la variable $j es "2", entonces quedará 
$DAY_HOUR["Mar"] que se convertirá en $LV[2].

*/
          $price += getPrice($room,$DAY_HOUR[$dayname][$j]);
          if($j == $end_hr - 7 && $i == $days - 1){ break; }
        }
      }
      else {
        for($j = 0; $j < 14; $j++) {
/*

En este otro caso, sucede algo muy similar a lo explicado previamente, pero en la función getPrice($room,$type), 
se pasa como valor del parámetro $type al arreglo $DAY_HOUR[] que lleva un índice que deriva del arreglo $DAYS, 
que a su vez tiene un índice determinado por el valor de la variable $start_iter, que a su vez este tendrá 
asignado un índice determinado por el valor de la variable $j. De esta forma, se continuará iterando exactamente 
en el día e índices correspondientes cuando el valor de la variable $i dejó de ser 0.

*/
          $price += getPrice($room,$DAY_HOUR[$DAYS[$start_iter]][$j]);
          if($j == $end_hr - 7 && $i == $days - 1){ break; }
        }
      }
//Si el valor de la variable $start_iter llega a ser igual a 6 (que es el número máximo de índices posibles en el arreglo $DAYS[]), se le asignará el valor de -1 para que pueda seguir iterando, empezando desde el 0 (número de índice del día lunes).
      if($start_iter == 6){ $start_iter =- 1; }
    }
  }
  return $price;
}

function calculate_Interval ($room, $days, $dayname, $init_hr, $end_hr) {
	
			/*
			 DESCRIPCIÓN:
			 La función calculate_Interval() recibe un tipo de área a rentar, los días a rentar, el nombre del día de
			 comienzo, una hora de inicio y una hora de término. Calcula el mismo intervalo de horas a través de un
			 derterminado número de días.
			 FUNCIONES INCLUÍDAS:
			 Esta función hace uso de la función calculate_Range().
			 RETORNO:
			 Regresará null si se ingresa un 0 en el número de días o sí es un sólo día y la hora de comienzo es
			 mayor a la de término.
			 Regresará un valor numérico entero con el valor representativo de los horarios calculados para los
			 intervalos seleccionados de la forma:
			 (Hora de inicio) -[intervalo]-> (Hora de término)
		 */
	
//EL arreglo $DAYS contiene todos los días de la semana.
  $DAYS = Array("Lun","Mar","Mie","Jue","Vie","Sab",'Dom');
//La variable $start_iter contiene como valor a la función de arrays integrada array_search(), teniendo esta como parámetros a la variable $dayname (que a su vez es un parámetro de la función actual) y al arreglo $DAYS.
  $start_iter = array_search($dayname,$DAYS);
//La variable $price siempre tiene que poseer un valor de 0 por defecto.
  $price = 0;
 
//La primera condición es, si por alguna razón la variable $days posee un valor de 1 y la variable $init_hr tiene un valor mayor que el de la variable $end_hr, o el valor de la variable $days es igual a 0, llamar a la función validateData() con el parámetro "hour".
  if($days == 1 && $init_hr > $end_hr || $days == 0) {  validateData('hour'); }
	
/*

De no cumplirse la primera condición, significa que todos los datos son coherentes para ser usados 
en cálculos. En este caso, si el valor de la variable $days es igual a 1 y el valor de la variable 
$dayname es igual a "Sab" y el valor de la variable $init_hr es mayor o igual a 15 y el de la variable 
$end_hr es menor o igual a 20, se le asigna como valor al resultado de la llamada de la función calculate_Range($room,$days,$dayname,$init_hr,$end_hr), en donde el parámetro $room es el valor del 
parámetro $room de esta función; $days es igual a 1 porque nada más se iterará el propio sábado, y 
$init_hr y $end_hr vienen siendo el valor de los parámetros $init_hr y $end_hr de esta misma función. 
Al final se retorna el valor de la variable $price.

*/
  else if($days == 1 && $dayname == 'Sab' && $init_hr >= 15 && $end_hr <= 20) { $price += calculate_Range($room,1,'Sab',$init_hr,$end_hr); return $price; }
	
/*

Este siguiente caso es similar al de arriba: Si el valor de la variable $days es igual a 1 y el valor de la variable $init_hr es igual al de la variable $end_hr menos uno (es decir, idénticos, e.g: 7 = (8 - 1)), se le asigna a la variable $price como valor al resultado de la llamada de la función calculate_Range($room,$days,$dayname,$init_hr,$end_hr) de la misma manera que arribva, pero el valor del parámetro $dayname sería "HR", pues nada más se calcularía una hora sencilla.

*/
  else if($days == 1 && $init_hr == $end_hr - 1) {  $price += calculate_Range($room,1,'HR',$init_hr,$end_hr); return $price;  }
  
//El caso por default es que se trate de cotizar un rango de días, y para ello se empleará un loop FOR que, habiendo prestado atención al par de casos anterior, debe ser entendible tal cual.
  for($i = 0;$i < $days; $i++, $start_iter++) {
    $price += calculate_Range($room,1,$DAYS[$start_iter],$init_hr,$end_hr);

//Si el valor de la variable $start_iter llega a ser igual a 6 (que es el número máximo de índices posibles en el arreglo $DAYS[]), se le asignará el valor de -1 para que pueda seguir iterando, empezando desde el 0 (número de índice del día lunes).
    if($start_iter == 6){ $start_iter =- 1; }
  }
  
  return $price;
}

?>